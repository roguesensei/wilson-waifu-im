import discord
import wilson.util.helpers as h

from discord import app_commands
from discord.ext import commands
from enum import Enum
from typing import Optional
from wilson.bot import Wilson
from wilson.util.http import Http

http = Http('https://api.waifu.im')


def get(url: str):
    r = http.get(url)
    return r.json()


class WaifuIm(commands.Cog):
    class WaifuTag(Enum):
        maid = 'maid'
        waifu = 'waifu'
        marin_kitagawa = 'marin-kitagawa'
        mori_calliope='mori-calliope'
        oppai='oppai'
        selfies='selfies'
        uniform='uniform'

    class WaifuTagNsfw(Enum):
        ass = 'ass'
        hentai = 'hentai'
        milf = 'milf'
        oral = 'oral'
        paizuri = 'paizuri'
        ecchi = 'ecchi'
        ero = 'ero'
        maid = 'maid'
        waifu = 'waifu'
        marin_kitagawa = 'marin-kitagawa'
        mori_calliope = 'mori-calliope'
        oppai = 'oppai'
        selfies = 'selfies'
        uniform = 'uniform'
    
    def __init__(self, bot: Wilson):
        self._bot = bot

    @app_commands.command(name='waifu', description='Make a call to Waifu.im')
    @app_commands.describe(tag='Waifu.im tag to search', gif='Whether or not response image should be a GIF')
    @app_commands.guild_only()
    async def waifu(self, interaction: discord.Interaction, tag: WaifuTag, gif: Optional[bool] = False):
        embed = self._get_img(interaction.user, tag.value, gif)
        if embed is not None:
            await interaction.response.send_message(embed=embed)
        else:
            await interaction.response.send_message('An error has occured, your search criterea may not exists on Waifu.im')
    
    @app_commands.command(name='waifu_nsfw', description='Make a call to Waifu.im - NSFW varient', nsfw=True)
    @app_commands.describe(tag='Waifu.im tag to search', gif='Whether or not response image should be a GIF')
    @app_commands.guild_only()
    async def waifu_nsfw(self, interaction: discord.Interaction, tag: WaifuTagNsfw, gif: Optional[bool] = False):
        embed = self._get_img(interaction.user, tag.value, gif, True)
        if embed is not None:
            await interaction.response.send_message(embed=embed)
        else:
            await interaction.response.send_message('An error has occured, your search criterea may not exists on Waifu.im')

    def _get_img(self, author: discord.Member, tag: str, gif : bool, nsfw: bool = False) -> discord.Embed:
        embed = None
        imgs = get(
            f'/search/?included_tags={tag}&gif={str(gif).lower()}&is_nsfw={str(nsfw).lower()}')
        if 'images' in imgs:
            title = tag.replace('-', ' ').title()
            img = imgs['images'][0]['url']
            embed = self._bot.generate_embed(f'{title}!', author, image_url=img)

        return embed
    # @commands.command(aliases=['titfuck'])
    # @commands.is_nsfw()
    # async def paizuri(self, ctx: commands.Context):
    #     img = get('/search/?included_tags=paizuri&gif=true')['images'][0]['url']

    #     embed = self._bot.generate_embed('Paizuri!', ctx.author, image_url=img)
    #     await ctx.send(embed=embed)


async def setup(bot: Wilson):
    await bot.add_cog(WaifuIm(bot))
